/**
 * Grunt Task Wrangler
 *
 * @copyright Copyright © 2018 Pageworks
 * @license   Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt)
{
    var path = require('path');


    require('load-grunt-config')(grunt, {
        configPath: path.join(process.cwd(), 'build/grunt/config'),
        data: {
            paths: {
                grunt: 'build/grunt',
                npm:   'node_modules',
                js: {
                    src:  'app/scripts',
                    dist: 'public/assets/scripts'
                },
                css: {
                    src:  'app/sass',
                    dist: 'public/assets/styles'
                },
                img: {
                    src:  'app/images',
                    dist: 'public/assets/images'
                }
            }
        }
    });
};
