import { APP_NAME, isDebug } from '../utils/environment';
import AbstractModule from './AbstractModule';

const MODULE_NAME = 'reCAPTCHA';
const EVENT_NAMESPACE = `${APP_NAME}.${MODULE_NAME}`;

const EVENT = {
    CLICK: `click.${EVENT_NAMESPACE}`
};

export default class extends AbstractModule {
    constructor(options) {
        super(options);

        // Declaration of properties
        if(isDebug) console.log('🔨 [module]:constructor - '+MODULE_NAME);
        this.$form      = this.$el.find('.js-form');
        this.key        = this.$form.data('key');
        this.CSRFtoken  = this.$form.find('input[name="CRAFT_CSRF_TOKEN"]').val();
    }

    init() {
        /**
         * Gets googles recaptcha API if it's undefined
         */
        let promise = new Promise((resolve, reject) => {
            if(typeof grecaptcha === 'undefined'){
                $.ajax({
                    url: 'https://www.google.com/recaptcha/api.js?render='+this.key,
                    dataType: 'script',
                    success: ()=>{
                        resolve();
                    },
                    error: (e)=>{
                        reject(e);
                    }
                });
            }else{
                resolve();
            }
        });

        /**
         * Handle reCAPTCHA verification
         * When grecaptcha is ready execute using our public key
         * when the execute promise is resolved store the token as data-token
         */
        promise.then(()=>{
            grecaptcha.ready(()=>{
                grecaptcha.execute(this.key, {action: 'homepage'})
                .then((token) => {
                    this.$form.data('token', token);
                });
            });
        })
        .catch((error)=>{
            console.log('Error:');
            console.log(error);
        });

        /**
         * Handle when the user submits the form
         */
        this.$form.on('submit', ()=>{
            new Promise((resolve, reject) => {
                $.ajax({
                    type: 'post',
                    cache: false,
                    data: {
                        'token': this.$form.data('token'),
                        'CRAFT_CSRF_TOKEN': this.CSRFtoken
                    },
                    url: '/recaptcha/verify',
                    success: (response)=>{ resolve(JSON.parse(response)); },
                    error: (e)=>{ reject(e); }
                });
            })
            .then((response)=>{
                console.log(response);
                if(response.status === 1){
                    console.log('Use Verified');
                    // handle form ajax
                }
                else console.log(response.message);
            })
            .catch((error)=>{
                console.log('Error:');
                console.log(error);
            });
        });
    }

    destroy() {
        this.$form.off('submit');
        super.destroy(isDebug, MODULE_NAME, EVENT_NAMESPACE);
    }
}