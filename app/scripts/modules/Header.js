import { APP_NAME, isDebug } from '../utils/environment';
import AbstractModule from './AbstractModule';

const MODULE_NAME = 'Header';
const EVENT_NAMESPACE = `${APP_NAME}.${MODULE_NAME}`;

const EVENT = {
    CLICK: `click.${EVENT_NAMESPACE}`
};

export default class extends AbstractModule {
    constructor(options) {
        super(options);

        // Declaration of properties
        if(isDebug) console.log('🔨 [module]:constructor - '+MODULE_NAME);
        this.$links = this.$el.find('.js-link');
    }

    init() {
        let clearLinks = ()=>{
            this.$links.each(function(){
                $(this).removeClass('u-visible');
            });
        }

        this.$links.on('click', function(){
            clearLinks();
            $(this).addClass('u-visible');
        });
    }

    destroy() {
        super.destroy(isDebug, MODULE_NAME, EVENT_NAMESPACE);
    }
}