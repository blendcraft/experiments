import { APP_NAME, isDebug } from '../utils/environment';
import AbstractModule from './AbstractModule';
import { resolve } from 'url';

const MODULE_NAME = 'Form';
const EVENT_NAMESPACE = `${APP_NAME}.${MODULE_NAME}`;

const EVENT = {
    CLICK: `click.${EVENT_NAMESPACE}`
};

export default class extends AbstractModule {
    constructor(options) {
        super(options);

        // Declaration of properties
        if(isDebug) console.log('🔨 [module]:constructor - '+MODULE_NAME);
        this.$form = this.$el;
    }

    init() {
        /**
         * Create a promise and resolve it with our forms response
         * This is to test and understand the basics of using a
         * promise as a callback
         * @tutorial https://developers.google.com/web/fundamentals/primers/promises
         */
        this.$form.on('submit', (e)=>{
            e.preventDefault();
            const $form = this.$form;
            const data = $form.serialize();

            let promise = new Promise((resolve, reject) => {
                $.post({
                    url:    '/',
                    dataType: 'json',
                    data:   data,
                    success: (e)=>{
                        resolve(e);
                    },
                    error: ()=>{
                        reject('something went horribly wrong');
                    }
                });
            });

            promise.then((message)=>{
                console.log('Success: ' + message.success);
                $form[0].reset();
            })
            .catch((error)=>{
                console.log('Error: ' + error);
            });
        });
    }

    destroy() {
        super.destroy(isDebug, MODULE_NAME, EVENT_NAMESPACE);
    }
}