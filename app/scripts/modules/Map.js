import { APP_NAME, isDebug } from '../utils/environment';
import AbstractModule from './AbstractModule';

const MODULE_NAME = 'Map';
const EVENT_NAMESPACE = `${APP_NAME}.${MODULE_NAME}`;

const EVENT = {
    CLICK: `click.${EVENT_NAMESPACE}`
};

export default class extends AbstractModule {
    constructor(options) {
        super(options);

        // Declaration of properties
        if(isDebug) console.log('🔨 [module]:constructor - '+MODULE_NAME);

    }

    init() {
        /**
         * Gets Google Maps API when we need it so there is no race condition
         * If google is already defined resolve promise
         * If google is undefined create a promise to resolve or reject when we get our response
         */
        let promise = new Promise((resolve, reject) => {
            if(typeof google === 'undefined'){
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyASofM16KR0cU2RlPQiCyiMKpgCCe60pYQ',
                    dataType: 'script',
                    success: ()=>{
                        resolve();
                    },
                    error: (e)=>{
                        reject(e);
                    }
                });
            }else{
                resolve();
            }
        });

        /**
         * When our promise is resolved or rejected we need to handle it
         * On resolve create our map
         * On reject display any error returned
         * @tutorial https://developers.google.com/maps/documentation/javascript/adding-a-google-map
         */
        promise.then(()=>{
            console.log('Success!');
            // The location of Uluru
            let uluru = {lat: -25.344, lng: 131.036};
            // The map, centered at Uluru
            let map = new google.maps.Map($('.js-map')[0], {zoom: 4, center: uluru});
            // The marker, positioned at Uluru
            let marker = new google.maps.Marker({position: uluru, map: map});
        })
        .catch((error)=>{
            console.log('Error: ' + error);
        });
    }

    destroy() {
        super.destroy(isDebug, MODULE_NAME, EVENT_NAMESPACE);
    }
}