// ==========================================================================
// Tools / Mixins
// ==========================================================================

//
// Set the color of the highlight that appears over a link while it's being tapped.
//
// By default, the highlight is suppressed.
//
// @param  {Color} $value [rgba(0, 0, 0, 0)] - The value of the highlight.
// @output `-webkit-tap-highlight-color`
//
@mixin tap-highlight-color($value: rgba(0, 0, 0, 0))
{
    -webkit-tap-highlight-color: $value;
}

//
// Set whether or not touch devices use momentum-based scrolling for the given element.
//
// By default, applies momentum-based scrolling for the current element.
//
// @param  {String} $value [rgba(0, 0, 0, 0)] - The type of scrolling.
// @output `-webkit-overflow-scrolling`
//
@mixin overflow-scrolling($value: touch)
{
    -webkit-overflow-scrolling: $value;
}

//
// Micro clearfix rules for containing floats.
//
// @link   http://www.cssmojo.com/the-very-latest-clearfix-reloaded/
// @param  {List} $supports The type of clearfix to generate.
// @output Injects `:::after` pseudo-element.
//
@mixin u-clearfix($supports...)
{
    &::after
    {
        display: if(list-contains($supports, table), table, block);
        clear: both;

        content: if(list-contains($supports, opera), ' ', '');
    }
}

//
// Vertically-center the direct descendants of the current element.
//
// Centering is achieved by displaying children as inline-blocks. Any whitespace
// between elements is nullified by redefining the font size of the container
// and its children.
//
// @output `font-size`, `display`, `vertical-align`
//
@mixin o-vertical-center
{
    font-size: 0;

    &::before
    {
        display: inline-block;

        height: 100%;

        content: '';
        vertical-align: middle;
    }

    > *
    {
        font-size: 1rem;

        display: inline-block;

        vertical-align: middle;
    }
}

//
// Generate `:hover` and `:focus` styles in one go.
//
// @link    https://github.com/inuitcss/inuitcss/blob/master/tools/_tools.mixins.scss
// @content Wrapped in `:focus` and `:hover` pseudo-classes.
// @output  Wraps the given content in `:focus` and `:hover` pseudo-classes.
//
@mixin u-hocus
{
    &:focus,
    &:hover
    {
        @content;
    }
}

//
// Generate `:active` and `:focus` styles in one go.
//
// @see     {Mixin} u-hocus
// @content Wrapped in `:focus` and `:active` pseudo-classes.
// @output  Wraps the given content in `:focus` and `:hover` pseudo-classes.
//
@mixin u-actus
{
    &:focus,
    &:active
    {
        @content;
    }
}

//
// Injects generic rules for disabling UL/OL/LI styles.
//
// @output `list-style`, `margin`, `padding`
//
@mixin u-list-reset
{
    margin: 0;
    padding: 0;

    list-style: none;
}

//
// Prevent text from wrapping onto multiple lines for the current element.
//
// An ellipsis is appended to the end of the line.
//
// 1. Ensure that the node has a maximum width after which truncation can occur.
// 2. Fix for IE 8/9 if `word-wrap: break-word` is in effect on ancestor nodes.
//
// @param  {Number} $width [100%] - The maximum width of element.
// @output `max-width`, `word-wrap`, `white-space`, `overflow`, `text-overflow`
//
@mixin u-truncate($width: 100%)
{
    overflow: hidden;

    white-space: nowrap;
    text-overflow: ellipsis;
    word-wrap: normal;  // [2]
    @if $width
    {
        max-width: $width; // [1]
    }
}

//
// Applies accessible hiding to the current element.
//
// @param  {Boolean} $important [true] - Whether the visibility is important.
// @output Properties for removing the element from the document flow.
//
@mixin u-accessibly-hidden($important: true)
{
    position: absolute $important;

    overflow: hidden;
    clip: rect(0 0 0 0);

    width: 1px;
    height: 1px;
    margin: 0;
    padding: 0;

    border: 0;

    $important: important($important);
}

//
// Allows an accessibly hidden element to be focusable via keyboard navigation.
//
// @content For styling the now visible element.
// @output  Injects `:focus`, `:active` pseudo-classes.
//
@mixin u-accessibly-focusable
{
    @include u-actus
    {
        clip: auto;

        width: auto;
        height: auto;

        @content;
    }
}

//
// Hide the current element from all.
//
// The element will be hidden from screen readers and removed from the document flow.
//
// @link   http://juicystudio.com/article/screen-readers-display-none.php
// @param  {Boolean} $important [true] - Whether the visibility is important.
// @output `display`, `visibility`
//
@mixin u-hidden($important: true)
{
    display: none   $important;
    visibility: hidden $important;

    $important: important($important);
}

//
// Show the current element for all.
//
// The element will be accessible from screen readers and visible in the document flow.
//
// @param  {String}  $display   [block] - The rendering box used for the element.
// @param  {Boolean} $important [true]  - Whether the visibility is important.
// @output `display`, `visibility`
//
@mixin u-shown($display: block, $important: true)
{
    display: $display $important;
    visibility: visible  $important;

    $important: important($important);
}

//
// Applies flexbox based modifiers. Typically used for container and wrapper objects.
//
// @output Several flexbox based modifiers
//
@mixin u-flexbox{
    &.-flex{
        display: inline-flex;
    }

    &.-flexbox{
        display: flex;
    }
    
    &.-flex,
    &.-flexbox{
        flex-wrap: wrap;
        justify-content: flex-start;
        align-items: flex-start;

        &.-no-wrap{
            flex-wrap: nowrap;
        }

        // Alignment
        &.-align-end{
            align-items: flex-end;
        }

        &.-align-center{
            align-items: center;
        }

        // Justify
        &.-justify-end{
            justify-content: flex-end;
        }

        &.-justify-center{
            justify-content: center;
        }
    
        &.-space-between{
            justify-content: space-between;
        }

        &.-space-around{
            justify-content: space-around;
        }

        // Utilities
        &.-full-center{
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        &.-col-reverse{
            flex-direction: column-reverse;
        }

        &.-row-reverse{
            flex-direction: row-reverse;
        }
    }
}

//
// Sets elements height to be 100%. Typically used on container and warpper objects
// to convert them to be fullscreen.
//
// @output .-full and .-fullscreen
//
@mixin u-fullscreen{
    &.-full{
        height: 100%;
    }

    &.-fullscreen{
        height: 100vh;
    }
}

@mixin u-mobile-padding{
    &.u-mobile-padding{
        @media (max-width: $from-huge){
            padding-left: $unit-small !important;
            padding-right: $unit-small !important;
        }
    }
}