module.exports = {
    options: {
        open      : false,
        proxy     : 'testing.craft.local',
        port      : 3000,
        watchTask : true,
        notify    : true
    },
    dev: {
        bsFiles: {
            src : [
                '<%= paths.css.dist %>/**/*.css',
                '<%= paths.js.dist %>/**/*.js',
                '<%= paths.img.dist %>/**/*.svg',
                '**/*.php',
                '**/*.html',
            ]
        }
    }
};
