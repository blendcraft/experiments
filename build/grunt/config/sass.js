module.exports = {
    options: {
        sourceMap   : true,
        outputStyle : 'expanded'
    },
    prod: {
        files: [
            {
                expand : true,
                cwd    : '<%= paths.css.src %>',
                src    : [ '**/*.scss' ],
                dest   : '<%= paths.css.dist %>',
                ext    : '.css'
            }
        ]
    }
};
