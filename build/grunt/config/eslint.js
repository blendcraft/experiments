module.exports = {
    options: {
        quiet      : false,
        configFile : '.eslintrc'
    },
    dev: [
        '<%= paths.js.src %>/**/*.js',
        '!<%= paths.js.src %>/vendors/**/*.js'
    ]
};
